package com.methodot.javaweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {

//    请修正下面错误的"Hello, word"的拼写！
//    请在修改完成后，通过点击左边菜单栏的「源代码管理」提交、同步代码。
//    同步代码完成后，请回到「应用工厂」，在「发布」界面中对已发布的应用进行版本升级，待应用发布完成后，即可看到修改后的效果。
//
//    错误在这里 ——————————————————————————
//                                       |
//                                       V
    public static String msg = "Hello, world!";

    @RequestMapping("/")
    public ModelAndView index(ModelAndView modelAndView){
        modelAndView.setViewName("index");
        modelAndView.addObject("msg", msg);
        if(msg.equals("Hello, word!")) {
            modelAndView.addObject("body", "恭喜您成功发布了Java示例项目... 啊！word 拼写错误，尝试修改代码解决这个问题吧。");
            modelAndView.addObject("btncontent", "修改我的代码");
            modelAndView.addObject("WebIDE", System.getenv("WEBIDE"));
        } else {
            modelAndView.addObject("body", "恭喜您成功修复了代码！您可以通过创建一个低代码项目来体验 Methodot 一站式云原生在线开发协作平台更丰富的功能。");
            modelAndView.addObject("btncontent", "创建一个低代码项目");
            modelAndView.addObject("WebIDE", "https://factory.methodot.com/home/projects");
        }
        return modelAndView;
    }
}
